# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.0.6] - 2019-03-18
### Added 
- 41 new files
- LICENSE
- CHANGELOG.md
- list.sh script generator

### Changed
- 98 updates

### Removed
- 4 files

### Bugfix
- 9 files

## [1.0.5] - 2018-01-03
### Added 
- 6 new files

### Changed  
- 13 updates

### Removed
- 3 files 

## [1.0.4] - 2017-12-07
### Added 
- 17 new files

### Changed
- 11 updates

### Removed 
- 2 files 

## [1.0.3] - 2017-10-25
### Added
- 26 new files
1. [type]:
    - Rectangle tube = TRE
    - Square tube = TCA
    - Round tube  = TRO
    - Oval tube = TOVAL
    - Angle tube =  TCOR
    - Adige tube = TBETA

2. [Section]:
	- Sections of the tubes.

3. [Material]:
 	* Steel
 	* Nitrogen

4. [Gas]:
	* n2
 	* o2
 	
- Quick search of parameters.
  - list_n2.md
  - list_o2.md
  - list_tca_o2.md
  - list_tca_n2.md
  - list_tca_inox.md
  - list_tre_o2.md
  - list_tre_n2.md
  - list_tre_inox.md
  - list_tro_o2.md
  - list_tro_n2.md
  - list_tro_inox.md

### Changed 
- 74 updates

## [1.0.2] - 2017-07-22
### Added
- 14 new files

### Changed
- 2 updates

## [1.0.1] - 2017-05-21

### Added
- README.md
- 38 new files

## [1.0] - 2016-11-09

### Added
- Basic backup initial


[Unreleased]: https://bitbucket.org/ccomin/cuttingparameterslaser/branches/compare/master%0Dv1.0.6
[1.0.6]: https://bitbucket.org/ccomin/cuttingparameterslaser/branches/compare/v1.0.6%0Dv1.0.5#diff
[1.0.5]: https://bitbucket.org/ccomin/cuttingparameterslaser/branches/compare/v1.0.5%0Dv1.0.4#diff
[1.0.4]: https://bitbucket.org/ccomin/cuttingparameterslaser/branches/compare/v1.0.4%0Dv1.0.3#diff
[1.0.3]: https://bitbucket.org/ccomin/cuttingparameterslaser/branches/compare/v1.0.3%0Dv1.0.2#diff
[1.0.2]: https://bitbucket.org/ccomin/cuttingparameterslaser/branches/compare/v1.0.2%0Dv1.0.1#diff
[1.0.1]: https://bitbucket.org/ccomin/cuttingparameterslaser/branches/compare/v1.0.1%0Dv1.0#diff
[1.0]: https://bitbucket.org/ccomin/cuttingparameterslaser/src/v1.0/
